<?php  

require_once('animal.php');

class ape extends animal
{
	
	function __construct($name)
	{
		$this->name = $name;
		$this->legs = 2;
	}

	public function yell()
	{
		return 'Auooo';
	}
}

?>