<?php  

require_once('animal.php');

class frog extends animal
{
	function __construct($name)
	{
		$this->name = $name;
	}

	public function jump()
	{
		return 'hop hop';
	}
}

?>